var express = require('express');
var router = express.Router();

// Require controller modules.
var ticket_controller = require('../controllers/TicketController');
var user_controller = require('../controllers/UserController');

/// TICKET ROUTES ///

router.get('/tickets/open', ticket_controller.open_ticket_list);

router.get('/tickets/closed', ticket_controller.closed_ticket_list);

router.get('/ticket/status/:ticketId', ticket_controller.ticket_status);

router.post('/ticket/update/:ticketId', ticket_controller.update_ticket_status);

router.post('/server/reset', ticket_controller.reset_server);


/// USER ROUTES ///

router.get('/user/details/:ticketId', user_controller.user_details);

module.exports = router;