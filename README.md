# BUS TICKETING SYSTEM 

## Overview 

Application to manage the bookings for a 40-seater bus, built in Node.js using Express framework and uses MongoDB as the database. 
The postman collection can be found at this [link](https://www.getpostman.com/collections/dac1626cb4a8fe106f97)

## Table of Contents 

- [BUS TICKETING SYSTEM](#BUS-TICKETING-SYSTEM)
- [Overview](#overview)
	- [API documentation](#api-documentation)
    - [Ticket Structure](#ticket-structure)
    - [Version](#version)
    - [Host Details](#host-details)
	- [API Call Structure](#api-call-structure)
	- [Response Structure](#response-structure)


### [API documentation]

The application has 6 apis, each for:

- Updating the ticket status (open/close + adding user details) 

-  Viewing Ticket Status

- Viewing all closed tickets

-  Viewing all open tickets

-  Viewing Details of person owning the ticket

-  Additional API for admin to reset the server (opens up all the tickets)



### [Ticket Structure]

The tickets are of the format as per below, with only the last 2 chars varying, which represent the seat which the ticket is for.

 - *5dfa70e1c171c3412314d4_01_*

- *5dfa70e1c171c3412314d4_02_*

and so on, until..

- *5dfa70e1c171c3412314d4_39_*

- *5dfa70e1c171c3412314d4_40_*


### [Version]
1.0.0

### Host Details ###

The application is hosted on Amazon EC2 instance.
Host name - `http://ec2-3-22-114-166.us-east-2.compute.amazonaws.com`
Port - `4000`

### API Call & Response Structure

-  `POST '/ticket/update/5dfa70e1c171c3412314d405'`

This API is used to update the status of the ticket to either open/closed along with updating the user details.

```
payload:
{
	"is_booked": "true",
    "userId": "5dfa70e1c171c3412314d55d",
    "username": "Saraswati C",
    "gender": "F",
    "userPhone": "123456789"
}
```

```
response (success):
{
    "is_booked": true,
    "date": "2020-05-30T13:16:44.930Z",
    "_id": "5dfa70e1c171c3412314d405",
    "passenger": "5dfa70e1c171c3412314d55d",
    "seat_number": 5
}
```
-  `GET 'booking/ticket/status/5dfa70e1c171c3412314d405'`

This API is used to get the status of the ticket, whether true/false, representing open/closed respectively.

```
response (success):
{
    "status": true
}
```


-  `GET '/booking/tickets/closed'`

This API gives the list of all the tickets that are in closed state; i.e. booked

```
response (success):
[
    {
        "_id": "5dfa70e1c171c3412314d401"
    },
    {
        "_id": "5dfa70e1c171c3412314d402"
    }
]
```


- `GET '/booking/tickets/open'`

This API gives the list of all the tickets that are in open state; i.e. not booked

```
response (success):
[
    {
        "_id": "5dfa70e1c171c3412314d401"
    },
    {
        "_id": "5dfa70e1c171c3412314d402"
    }
]
```

-  `GET booking/user/details/5dfa70e1c171c3412314d405`

This API gives the details of the person owning the ticket with ticketId as in the request param

```
response (success):
{
    "_id": "5dfa70e1c171c3412314d55d",
    "name": "Saraswati C",
    "gender": "F",
    "phone": "123456789",
    "__v": 0
}
```

-  `POST /server/reset`

This API will be used by admin to reset the server and hence, open/unbook all the tickets

```
payload:

{
  "username": "ubuntu",
}
```
```
response (success):
{
    "message": "success"
}
```    

### DB Schema ###

The DB used here is **MongoDB.** 
It consists of 2 collections - tickets and users.

A sample record of the **tickets** collection doc is as follows:

### For a booked ticket:
    {
      "_id": ObjectId("5dfa70e1c171c3412314d401)",
      "is_booked": true,
      "date": ISODate("2020-05-30T04:07:49.881Z"),
      "seat_number": 1,
      "passenger": ObjectId("5dfa70e1c171c3412314d11d")
    }

### For an unbooked ticket:
    {
      "_id": ObjectId("5dfa70e1c171c3412314d402"),
      "is_booked": false,
      "date": {
        "$date": {
          "$numberLong": "1576707116951"
        }
      },
      "seat_number": {
        "$numberInt": "2"
      },
      "passenger": null
    }


A sample record of the **users** collection doc is as follows:

    {
      "_id": ObjectId("5dfa70e1c171c3412314d11d"),
      "name": "Riddhi",
      "gender": "F",
      "phone": "12345",
      "__v": 0
    }

