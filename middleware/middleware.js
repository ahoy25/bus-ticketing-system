function openTicket(ticket) {
    ticket.is_booked = false
    ticket.passenger = null
    ticket.save()
        .then(data => console.log(`Opened ticket with ticketID: ${ticket._id}`))
        .catch(err => console.log(`Failed to open ticket with ticketID: ${ticket._id}`))
}

module.exports = {
    openTicket: openTicket,
}

