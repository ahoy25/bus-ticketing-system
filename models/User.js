const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const UserSchema = new mongoose.Schema({
    name: String,
    gender: String,
    phone: {type: String, unique: true},
});

UserSchema.plugin(uniqueValidator);
module.exports = mongoose.model('User', UserSchema);