var Ticket = require('../models/Ticket');
var User = require('../models/User');
const middleware = require('../middleware/middleware')
const openTicket = middleware.openTicket

exports.open_ticket_list = function (req, res) {
    var query = Ticket.find({is_booked: false}).select('_id');
    query.exec(function (err, result) {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
        }
    });
};

exports.closed_ticket_list = function (req, res) {
    var query = Ticket.find({is_booked: true}).select('_id');
    query.exec(function (err, result) {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
        }
    });
};

exports.ticket_status = function (req, res) {
    const {ticketId} = req.params
    Ticket.findById(ticketId, function (err, ticket) {
        if (err) res.status(404).json({message: err})
        if (ticket) res.status(200).json({status: ticket.is_booked})
    })
};

exports.update_ticket_status = function (req, res) {

    const {ticketId} = req.params
    const payload = req.body
    const passenger = payload.userId

    Ticket.findById(ticketId, function (err, ticket) {
        if (err) res.status(404).json({message: err})
        if (ticket) {
            ticket.date = Date.now()
            if (passenger) {
                ticket.is_booked = true
                ticket.passenger = passenger
                ticket.seat_number = ticketId.slice(-2)
                const user = new User(req.body.passenger)
                user._id = payload.userId
                user.name = payload.username
                user.gender = payload.gender
                user.phone = payload.userPhone
                user.save()
            } else {
                ticket.is_booked = false
                ticket.passenger = null
                const user_id = ticket.passenger
                User.remove({_id: user_id}, function (err) {
                    if (err) {
                        res.status(404).json(err)
                    }
                })
            }
            ticket.save()
                .then(data => res.status(200).json(data))
                .catch(err => res.status(404).json(err))
        }

    })

};

exports.reset_server = function (req, res) {
    if (!("username" in req.body)) {
        res.status(400).json({message: "admin username required!"})
    }

    const {username} = req.body

    if (!(username === process.env.USER)) {
        res.status(400).json({message: "username is incorrect"})
    }

    Ticket.find({}, (err, data) => {
        if (err) res.status(404).json({message: err})
        if (data) {
            data.forEach(openTicket)
            res.status(200).json({message: "success"})
        }
    })
};