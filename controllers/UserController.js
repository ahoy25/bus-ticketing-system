var Ticket = require('../models/Ticket');
var User = require('../models/User');

exports.user_details = function (req, res) {
    const {ticketId} = req.params
    Ticket.findById(ticketId, function (err, ticket) {
        if (err) res.status(404).json({message: err})
        if (ticket) {
            if(ticket.passenger != null) {
                User.findById(ticket.passenger, function (err, user) {
                    if (err) res.status(404).json({message: err})
                    if (user) res.status(200).json(user)
                })
            }
            else {
                res.status(200).json("Ticket open, User for this ticket is null")
            }
        }
    })
};